import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <nav>
        <div className="container">
          <div className="navbar__content">
            <div className="navbar__content__logo">
              <div className="navbar__content__logo__first" />
              <div className="navbar__content__logo__second" />
            </div>
            <div className="navbar__content__items">
              <ul>
                <li>
                  <Link to="/about">About</Link>
                </li>
                <li>
                  <Link to="#">Help</Link>
                </li>
                <li>
                  <Link to="#">Features</Link>
                </li>
                <li>
                  <Link to="#">Signup</Link>
                </li>
              </ul>
            </div>
            <button className="btn btn-primary navbar__content__btn">
              Request Demo
              <i class="fas fa-arrow-right"></i>
            </button>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
