import React from "react";
import { Link } from "react-router-dom";
import bg from "../../assets/rect.svg";

const FtCard = ({ img, title, description, to, bg_color }) => {
  return (
    <div className="FtCard">
      <div className="FtCard__content">
        <div className="FtCard__content__img">
          <img src={img} alt="FtCard" />
        </div>
        <div className="FtCard__content__bg">
          <svg
            width="434"
            height="358"
            viewBox="0 0 434 358"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              opacity="0.1"
              d="M44.2904 41.7355C48.3276 17.6466 69.1778 0.00012207 93.6026 0.00012207H383.285C410.899 0.00012207 433.285 22.3859 433.285 50.0001V308C433.285 335.614 410.899 358 383.285 358H50.3624C19.4471 358 -4.05989 330.226 1.05018 299.736L44.2904 41.7355Z"
              fill={bg_color}
            />
          </svg>
        </div>
        <div className="FtCard__content__vectorBg">
          <p className="FtCard__content__vectorBg__title">{title}</p>
          <p className="FtCard__content__vectorBg__description">
            {description}
          </p>
          <Link to={to}>
            Learn more <i class="fas fa-arrow-right"></i>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FtCard;
