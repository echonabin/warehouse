import React from "react";
import footerLogo from "../../assets/footerLogo.svg";
import fb from "../../assets/f.svg";
import insta from "../../assets/i.svg";
import twitter from "../../assets/t.svg";

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer__container">
        <div className="container">
          <div className="footer__container__top">
            <div className="footer__container__top__heading">
              <p className="footer__container__top__heading__title">
                Try for free!
              </p>
              <p className="footer__container__top__heading__subTitle">
                Get limited 1 week free try our features!
              </p>
            </div>
            <div className="footer__container__top__buttons">
              <button className="btn btn-pink">Learn More</button>
              <button className="btn btn-primary navbar__content__btn">
                Request Demo
                <i class="fas fa-arrow-right"></i>
              </button>
            </div>
          </div>
          <div className="footer__container__middle">
            <div className="footer__container__middle__left">
              <div className="footer__container__middle__left__logo">
                <img src={footerLogo} alt="footerLogo" />
              </div>
              <div className="footer__container__middle__left__address">
                <p className="footer__container__middle__left__address__title">
                  Warehouse Society, 234 Bahagia Ave Street PRBW 29281
                </p>
                <p className="footer__container__middle__left__address__email">
                  info@warehouse.project 1-232-3434 (Main)
                </p>
              </div>
            </div>
            <div className="footer__container__middle__center">
              <div className="footer__container__middle__center__left">
                <p className="footer__container__middle__center__left__title">
                  About
                </p>
                <a href="/#">Profile</a>
                <a href="/#">Features</a>
                <a href="/#">Careers</a>
                <a href="/#">DW News</a>
              </div>
              <div className="footer__container__middle__center__right">
                <p className="footer__container__middle__center__right__title">
                  Help
                </p>
                <a href="/#">Support</a>
                <a href="/#">Sign up</a>
                <a href="/#">Guide</a>
                <a href="/#">Reports</a>
                <a href="/#">Q&A</a>
              </div>
            </div>
            <div className="footer__container__middle__right">
              <p className="footer__container__middle__right__title">
                Social Media
              </p>
              <div className="footer__container__middle__right__socialIcons">
                <a
                  href="https://facebook.com/echonabin"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img src={fb} alt="facebook" />
                </a>
                <a
                  href="https://twitter.com/echonabin"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img src={twitter} alt="Twitter" />
                </a>
                <a
                  href="https://instagram.com/echo_nabin"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img src={insta} alt="Instagram" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
