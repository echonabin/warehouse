import React from "react";
import "./scss/main.scss";
import { Home } from "./pages";
const App = () => {
  return <Home />;
};

export default App;
