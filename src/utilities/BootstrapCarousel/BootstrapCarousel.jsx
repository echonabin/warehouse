import React from "react";
import Carousel from "react-bootstrap/Carousel";
import RectangleCard from "../RectangleCard/RectangleCard";
import image1 from "../../assets/image1.svg";
import image2 from "../../assets/image2.svg";
import image3 from "../../assets/image3.svg";

const BootstrapCarousel = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <RectangleCard
          image={image1}
          fullname="Jenny Doe"
          position="UI/UX Engineer"
          message="Suspendisse ultrices at diam lectus nullam. 
Nisl, sagittis viverra enim erat tortor ultricies massa turpis. Arcu pulvinar aenean nam laoreet nulla."
        />
      </Carousel.Item>
      <Carousel.Item>
        <RectangleCard
          image={image2}
          fullname="John Doe"
          position="Full Stacks Developer"
          message="Suspendisse ultrices at diam lectus nullam. 
Nisl, sagittis viverra enim erat tortor ultricies massa turpis. Arcu pulvinar aenean nam laoreet nulla."
        />
      </Carousel.Item>
      <Carousel.Item>
        <RectangleCard
          image={image3}
          fullname="Paul"
          position="Front-End Developer"
          message="Suspendisse ultrices at diam lectus nullam. 
Nisl, sagittis viverra enim erat tortor ultricies massa turpis. Arcu pulvinar aenean nam laoreet nulla."
        />
      </Carousel.Item>
    </Carousel>
  );
};

export default BootstrapCarousel;
