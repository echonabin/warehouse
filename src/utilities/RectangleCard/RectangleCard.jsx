import React from "react";

const RectangleCard = ({ children, image, fullname, position, message }) => {
  return (
    <div className="RectangleCard">
      <div className="RectangleCard__wrapper">
        <div className="RectangleCard__content__left">
          <img src={image} alt="icon" />
        </div>
        <div className="RectangleCard__content__right">
          <div className="RectangleCard__content__right__heading">
            <p className="RectangleCard__content__right__heading__name">
              {fullname}
            </p>
            <p className="RectangleCard__content__right__heading__title">
              {position}
            </p>
          </div>
          <p className="RectangleCard__content__right__description">
            {message}
          </p>
        </div>
      </div>
    </div>
  );
};

export default RectangleCard;
