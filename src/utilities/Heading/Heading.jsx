import React from "react";

const Heading = ({ title, description }) => {
  return (
    <div className="heading">
      <p className="heading__title">{title}</p>
      <p className="heading__description">{description}</p>
    </div>
  );
};

export default Heading;
