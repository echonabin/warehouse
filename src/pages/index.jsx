export { default as Home } from "./Home";
export { default as Banner } from "./Banner";
export { default as Testimonials } from "./Testimonials";
