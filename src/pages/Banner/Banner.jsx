import React from "react";
import banner from "../../assets/banner.svg";
import image2 from "../../assets/bimage2.svg";

const Banner = () => {
  return (
    <div className="banner">
      <div className="container">
        <div className="banner__content">
          <div className="banner__content__left">
            <p className="banner__content__left__heading">
              Save your data storage here.
            </p>
            <p className="banner__content__left__description">
              Data Warehouse is a data storage area that has been tested for
              security, so you can store your data here safely but not be afraid
              of being stolen by others.
            </p>
            <button className="btn btn-primary banner__content__left__btn">
              Learn more
            </button>
          </div>
          <div className="banner__content__right">
            <img
              src={banner}
              alt="banner"
              className="banner__content__right__image"
            />
          </div>
        </div>
        <div className="banner__card">
          <div className="banner__card__wrapper">
            <div className="banner__card__left">
              <img src={image2} alt="image2" />
            </div>
            <div className="banner__card__right">
              <p className="banner__card__right__title">
                We are a high-level data storage bank
              </p>
              <p className="banner__card__right__description">
                The place to store various data that you can access at any time
                through the internet and where you can carry it. This very
                flexible storage area has a high level of security. To enter
                into your own data you must enter the password that you created
                when you registered in this Data Warehouse.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
