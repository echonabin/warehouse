import React from "react";
import { RootLayout } from "../../layouts";
import Banner from "../Banner";
import Features from "../Features/Features";
import Testimonials from "../Testimonials";

const Home = () => {
  return (
    <RootLayout>
      <Banner />
      <Features />
      <Testimonials />
    </RootLayout>
  );
};

export default Home;
