import React from "react";
import BootstrapCarousel from "../../utilities/BootstrapCarousel/BootstrapCarousel";

const Testimonials = () => {
  return (
    <div className="Testimonials ">
      <div className="container">
        <div className="Testimonials__box">
          <p className="Testimonials__box__title">Testimonials</p>
          <div className="Testimonials__box__wrapper">
            <BootstrapCarousel />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Testimonials;
