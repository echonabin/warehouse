import React from "react";
import FtCard from "../../components/FtCard/FtCard";
import { Heading } from "../../utilities";

import feat1 from "../../assets/feat1.svg";
import feat2 from "../../assets/feat2.svg";
import feat3 from "../../assets/feat3.svg";
import feat4 from "../../assets/feat4.svg";

const Features = () => {
  return (
    <div className="features">
      <div className="container">
        <div className="features__content">
          <div className="features__content__heading">
            <Heading
              title="Features"
              description="Some of the features and advantages that we provide for those of you who store data in this Data Warehouse."
            />
          </div>
          <div className="features__content__cards">
            <FtCard
              img={feat1}
              title="Search Data"
              description="Don’t worry if your data is very large, the Data Warehoue provides a search engine, which is useful for making it easier to find data effectively saving time."
              to="#"
              bg_color="#68C9BA"
            />
            <FtCard
              img={feat2}
              title="24 Hours Access"
              description="Access is given 24 hours a full morning to night and
              meet again in the morning, giving you comfort when
              you need data when urgent."
              to="#"
              bg_color="#9C69E2"
            />
            <FtCard
              img={feat3}
              title="Print Out"
              description="Print out service gives you convenience if someday
              you need print data, just edit it all and just print it."
              to="#"
              bg_color="#F063B8"
            />
            <FtCard
              img={feat4}
              title="Security Code"
              description="Data Security is one of our best facilities. Allows for your files
              to be safer. The file can be secured with a code or password that 
              you created, so only you can open the file."
              to="#"
              bg_color="#2D9CDB"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Features;
